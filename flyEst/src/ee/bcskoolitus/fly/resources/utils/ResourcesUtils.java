package ee.bcskoolitus.fly.resources.utils;

import java.util.Date;

public abstract class ResourcesUtils {

    public static java.sql.Date javaDateToSqlDate(Date dateToConvert) {
        return new java.sql.Date(dateToConvert.getTime());
    }
}
