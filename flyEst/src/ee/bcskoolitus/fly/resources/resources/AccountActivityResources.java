package ee.bcskoolitus.fly.resources.resources;

import ee.bcskoolitus.fly.dao.AccountActivity;
import ee.bcskoolitus.fly.resources.utils.DatabaseConnection;
import ee.bcskoolitus.fly.resources.utils.ResourcesUtils;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public abstract class AccountActivityResources {

    public static List<AccountActivity> displayPersonalAccountHistory(String client) {
        List<AccountActivity> personalAccountHistory = new ArrayList<>();
        String sqlQuery = "SELECT * FROM account_activity WHERE account_activity_client  = '"
                + client + "' or account_activity_beneficiary  = '" + client + "'";
        try (Statement statement = DatabaseConnection.getConnection()
                .createStatement()) {
            ResultSet results = statement.executeQuery(sqlQuery);
            while (results.next()) {
                AccountActivity accountActivity = new AccountActivity();
                accountActivity.setAccountActivityDate(results.getDate("account_activity_date"));
                accountActivity.setAccountActivityClient(
                        results.getString("account_activity_client"));
                accountActivity.setAccountActivityBeneficiary(
                        results.getString("account_activity_beneficiary"));
                accountActivity.setAccountActivityAmount(
                        results.getBigDecimal("account_activity_amount"));
                accountActivity.setAccountActivityAccountBalanceAfter(
                        results.getBigDecimal("account_activity_account_balance_after"));
                accountActivity.setAccountActivityDetails(
                        results.getString("account_activity_details"));
                personalAccountHistory.add(accountActivity);
            }
        } catch (SQLException e) {
            System.out.println("Error on Selecting personal account history" + e.getMessage());
        }
        return personalAccountHistory;
    }

// Calculating balance after:

    public static AccountActivity addAccountActivity(AccountActivity newAccountActivity) {

        getPersonalInfoAccountBalanceAfter(newAccountActivity.getAccountActivityClient(), newAccountActivity.getAccountActivityBeneficiary(),
                newAccountActivity.getAccountActivityAmount());

        getAccountBalanceAfterForAccountActivityTable(newAccountActivity);

        return newAccountActivity;
    }

    private static void getPersonalInfoAccountBalanceAfter(String accountActivityClient, String accountActivityBeneficiary, BigDecimal accountActivityAmount) {
        String clientBalanceQuery = "SELECT personal_info_account_balance FROM personal_info WHERE personal_info_code_name = '" + accountActivityClient + "'";
        String beneficiaryBalanceQuery = "SELECT personal_info_account_balance FROM personal_info WHERE personal_info_code_name = '" + accountActivityBeneficiary + "'";
        try (Statement statement = DatabaseConnection.getConnection()
                .createStatement()) {

            ResultSet clientQuery = statement.executeQuery(clientBalanceQuery);
            clientQuery.next();
            BigDecimal clientBalanceAfter = clientQuery.getBigDecimal(1);
            BigDecimal clientNewAccountBalance = clientBalanceAfter.subtract(accountActivityAmount);

            ResultSet beneficiaryQuery = statement.executeQuery(beneficiaryBalanceQuery);
            beneficiaryQuery.next();
            BigDecimal beneficiaryBalanceAfter = beneficiaryQuery.getBigDecimal(1);
            BigDecimal beneficiaryNewAccountBalance = beneficiaryBalanceAfter.add(accountActivityAmount);

            String clientPersoBalanceUpdate = "UPDATE personal_info SET personal_info_account_balance = " + clientNewAccountBalance + " WHERE personal_info_code_name = '" + accountActivityClient + "'";
            String beneficiaryPersoBalanceUpdate = "UPDATE personal_info SET personal_info_account_balance = " + beneficiaryNewAccountBalance + " WHERE personal_info_code_name = '" + accountActivityBeneficiary + "'";

            Integer clientPersoBalanceFinal = statement.executeUpdate(clientPersoBalanceUpdate);
            Integer beneficiaryPersoBalanceFinal = statement.executeUpdate(beneficiaryPersoBalanceUpdate);
            if (clientPersoBalanceFinal == 1) {
                System.out.println("Success client balance!");
            } else {
                System.out.println("Client balance failed!");
            }
            if (beneficiaryPersoBalanceFinal == 1) {
                System.out.println("Success beneficiary balance!");
            } else {
                System.out.println("Beneficiary balance failed!");
            }

        } catch (SQLException e) {
            System.out.println("Error on updating account balance in personal info" + e.getMessage());
        }
    }

    private static void getAccountBalanceAfterForAccountActivityTable(AccountActivity newAccountActivity) {

        String sQuery = "SELECT personal_info_account_balance FROM personal_info WHERE personal_info_code_name = '" + newAccountActivity.getAccountActivityClient() + "'";

        try (Statement statement = DatabaseConnection.getConnection()
                .createStatement()) {

            ResultSet result = statement.executeQuery(sQuery);
            result.next();
            BigDecimal resultAfter = result.getBigDecimal(1);

            clientAccountBalanceUpdate(newAccountActivity, resultAfter);

        } catch (SQLException e) {
            System.out.println("Error on updating account balance in account activity");
        }

    }

    private static void clientAccountBalanceUpdate(AccountActivity newAccountActivity, BigDecimal resultAfter) {
        String sqlQuery = "INSERT INTO account_activity "
                + "(account_activity_client, account_activity_beneficiary, account_activity_amount, " +
                "account_activity_account_balance_after, account_activity_details, account_activity_date) " + "VALUES ('"
                + newAccountActivity.getAccountActivityClient() + "', '"
                + newAccountActivity.getAccountActivityBeneficiary() + "', "
                + newAccountActivity.getAccountActivityAmount() + ", "
                + resultAfter + ", '"
                + newAccountActivity.getAccountActivityDetails() + "', '"
                + ResourcesUtils.javaDateToSqlDate(newAccountActivity.getAccountActivityDate()) + "');";

        try (Statement statement = DatabaseConnection.getConnection()
                .createStatement()) {
            Integer resultCode = statement.executeUpdate(sqlQuery);
            if (resultCode == 1) {
                System.out.println("Success!");
            } else {
                System.out.println("you failed!");
            }

        } catch (SQLException e) {
            System.out.println("Error on adding new line in account activity table");
        }
    }
}






