package ee.bcskoolitus.fly.resources.resources;

import ee.bcskoolitus.fly.dao.PersonalInfo;
import ee.bcskoolitus.fly.resources.utils.DatabaseConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public abstract class PersonalInfoResources {

    public static PersonalInfo selectOnePerson(String name) {
        PersonalInfo personalInfo = null;
        String sqlQuery = "SELECT * FROM personal_info WHERE personal_info_first_name  = '"
                + name + "' or personal_info_last_name  = '"
                + name + "' or personal_info_code_name = '" + name + "'";
        try (Statement statement = DatabaseConnection.getConnection()
                .createStatement()) {
            ResultSet results = statement.executeQuery(sqlQuery);
            while (results.next()) {
                personalInfo = new PersonalInfo();
                personalInfo.setPersonalInfoCodeName(results.getString("personal_info_code_name"));
                personalInfo.setPersonalInfoFirstName(
                        results.getString("personal_info_first_name"));
                personalInfo.setPersonalInfoLastName(
                        results.getString("personal_info_last_name"));
                personalInfo.setPersonalInfoAccountBalance(
                        results.getBigDecimal("personal_info_account_balance"));
                personalInfo.setPersonalInfoGearCheck(
                        results.getBoolean("personal_info_gear_check"));
                personalInfo.setPersonalInfoExamPassed(
                        results.getBoolean("personal_info_exam_passed"));
            }
        } catch (SQLException e) {
            System.out.println("Error on Selecting personal information");
        }
        return personalInfo;
    }

    public static List<PersonalInfo> autocompleteName(String autoName) {
        List<PersonalInfo> allPersonalInfo = new ArrayList<>();
        String sqlQuery = "SELECT * FROM personal_info WHERE  " +
                "personal_info_code_name LIKE '" + autoName + "%' " +
                "or personal_info_first_name LIKE '" + autoName + "%' " +
                "or personal_info_last_name LIKE '" + autoName + "%' ";
        try (ResultSet results = DatabaseConnection.getConnection()
                .prepareStatement(sqlQuery).executeQuery()) {
            while (results.next()) {
                PersonalInfo personalInfo = new PersonalInfo();
                personalInfo.setPersonalInfoCodeName(results.getString("personal_info_code_name"));
                personalInfo.setPersonalInfoFirstName(
                        results.getString("personal_info_first_name"));
                personalInfo.setPersonalInfoLastName(
                        results.getString("personal_info_last_name"));
                personalInfo.setPersonalInfoAccountBalance(
                        results.getBigDecimal("personal_info_account_balance"));
                personalInfo.setPersonalInfoGearCheck(
                        results.getBoolean("personal_info_gear_check"));
                personalInfo.setPersonalInfoExamPassed(
                        results.getBoolean("personal_info_exam_passed"));
                allPersonalInfo.add(personalInfo);
            }
        } catch (SQLException e) {
            System.out.println(
                    " Error on getting autocompleteName: " + e.getMessage());
        }

        return allPersonalInfo;
    }


    public static PersonalInfo addPersonalInfo(PersonalInfo newPersonalInfo) {
        String sqlQuery = "INSERT INTO personal_info "
                + "(personal_info_code_name, personal_info_first_name, personal_info_last_name, personal_info_account_balance," +
                " personal_info_gear_check, personal_info_exam_passed) " + "VALUES ('"
                + newPersonalInfo.getPersonalInfoCodeName() + "', '"
                + newPersonalInfo.getPersonalInfoFirstName() + "', '"
                + newPersonalInfo.getPersonalInfoLastName() + "', "
                + newPersonalInfo.getPersonalInfoAccountBalance() + ", "
                + newPersonalInfo.getPersonalInfoGearCheck() + ", "
                + newPersonalInfo.getPersonalInfoExamPassed() + ");";
        try (Statement statement = DatabaseConnection.getConnection()
                .createStatement()) {
            Integer resultCode = statement.executeUpdate(sqlQuery);
            if (resultCode == 1) {
                System.out.println("Success");
            } else {
                System.out.println("you failed");
            }
        } catch (SQLException e) {
            System.out.println("Error on adding new person");
        }
        return newPersonalInfo;
    }

    public static boolean modifyPersonalInfo(PersonalInfo personalInfoUpdate) {
        boolean isUpdateSuccessful = false;
        String sqlQuery = "UPDATE personal_info SET personal_info_code_name='"
                + personalInfoUpdate.getPersonalInfoCodeName()
                + "', personal_info_first_name='"
                + personalInfoUpdate.getPersonalInfoFirstName()
                + "', personal_info_last_name='"
                + personalInfoUpdate.getPersonalInfoLastName()
                + "', personal_info_account_balance="
                + personalInfoUpdate.getPersonalInfoAccountBalance()
                + ", personal_info_gear_check="
                + personalInfoUpdate.getPersonalInfoGearCheck()
                + ", personal_info_exam_passed="
                + personalInfoUpdate.getPersonalInfoExamPassed()
                + " WHERE personal_info_code_name='"
                + personalInfoUpdate.getPersonalInfoCodeName() + "'";
        System.out.println(sqlQuery);
        try (Statement statement = DatabaseConnection.getConnection()
                .createStatement();) {
            Integer resultCode = statement.executeUpdate(sqlQuery,
                    Statement.RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                isUpdateSuccessful = true;
            }
        } catch (SQLException e) {
            System.out.println("Error on modifying course type");
        }
        return isUpdateSuccessful;
    }

   /*
    public static boolean searchIfUniqueCodeName(String personalInfoCodeName) {

        boolean isCodeNameUnique = false;
        String sqlQuery = "SELECT * FROM personal_info WHERE personal_info_code_name='"
                + personalInfoCodeName +"'";
        try (Statement statement = DatabaseConnection.getConnection()
                .createStatement();) {
            ResultSet result = statement.executeQuery(sqlQuery);
            result.last();      //viib meid viimasele reale
            if( result.getRow()== 0) {
                isCodeNameUnique= true;
            }
        } catch (SQLException e) {
            System.out.println("Error on creating unique code name");
        }
        System.out.println(isCodeNameUnique);
        return isCodeNameUnique;
    }

*/

}





