package ee.bcskoolitus.fly.dao;

import java.math.BigDecimal;
import java.util.Date;

public class AccountActivity {

    private Date accountActivityDate;
    private String accountActivityClient;
    private String accountActivityBeneficiary;
    private BigDecimal accountActivityAmount;
    private BigDecimal accountActivityAccountBalanceAfter;
    private String accountActivityDetails;


    public Date getAccountActivityDate() {
        return accountActivityDate;
    }

    public void setAccountActivityDate(Date accountActivityDate) {
        this.accountActivityDate = accountActivityDate;
    }

    public String getAccountActivityClient() {
        return accountActivityClient;
    }

    public void setAccountActivityClient(String accountActivityClient) {
        this.accountActivityClient = accountActivityClient;
    }

    public String getAccountActivityBeneficiary() {
        return accountActivityBeneficiary;
    }

    public void setAccountActivityBeneficiary(String accountActivityBeneficiary) {
        this.accountActivityBeneficiary = accountActivityBeneficiary;
    }

    public BigDecimal getAccountActivityAmount() {
        return accountActivityAmount;
    }

    public void setAccountActivityAmount(BigDecimal accountActivityAmount) {
        this.accountActivityAmount = accountActivityAmount;
    }

    public BigDecimal getAccountActivityAccountBalanceAfter() {
        return accountActivityAccountBalanceAfter;
    }

    public void setAccountActivityAccountBalanceAfter(BigDecimal accountActivityAccountBalanceAfter) {
        this.accountActivityAccountBalanceAfter = accountActivityAccountBalanceAfter;
    }

    public String getAccountActivityDetails() {
        return accountActivityDetails;
    }

    public void setAccountActivityDetails(String accountActivityDetails) {
        this.accountActivityDetails = accountActivityDetails;
    }

    @Override
    public String toString() {
        return "AccountActivity{" +
                "accountActivityDate=" + accountActivityDate +
                ", accountActivityClient='" + accountActivityClient + '\'' +
                ", accountActivityBeneficiary='" + accountActivityBeneficiary + '\'' +
                ", accountActivityAmount=" + accountActivityAmount +
                ", accountActivityAccountBalanceAfter=" + accountActivityAccountBalanceAfter +
                ", accountActivityDetails='" + accountActivityDetails + '\'' +
                '}';
    }
}

