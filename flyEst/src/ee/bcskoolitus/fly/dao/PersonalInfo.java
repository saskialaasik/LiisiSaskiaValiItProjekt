package ee.bcskoolitus.fly.dao;

import java.math.BigDecimal;

public class PersonalInfo {


    private String personalInfoCodeName;
    private String personalInfoFirstName;
    private String personalInfoLastName;
    private java.math.BigDecimal personalInfoAccountBalance;    // valuof()
    private Boolean personalInfoGearCheck;
    private Boolean personalInfoExamPassed;


    public String getPersonalInfoCodeName() {
        return personalInfoCodeName;
    }

    public void setPersonalInfoCodeName(String personalInfoCodeName) {
        this.personalInfoCodeName = personalInfoCodeName;
    }

    public String getPersonalInfoFirstName() {
        return personalInfoFirstName;
    }

    public void setPersonalInfoFirstName(String personalInfoFirstName) {
        this.personalInfoFirstName = personalInfoFirstName;
    }

    public String getPersonalInfoLastName() {
        return personalInfoLastName;
    }

    public void setPersonalInfoLastName(String personalInfoLastName) {
        this.personalInfoLastName = personalInfoLastName;
    }

    public BigDecimal getPersonalInfoAccountBalance() {
        return personalInfoAccountBalance;
    }

    public void setPersonalInfoAccountBalance(BigDecimal personalInfoAccountBalance) {
        this.personalInfoAccountBalance = personalInfoAccountBalance;
    }

    public Boolean getPersonalInfoGearCheck() {
        return personalInfoGearCheck;
    }

    public void setPersonalInfoGearCheck(Boolean personalInfoGearCheck) {
        this.personalInfoGearCheck = personalInfoGearCheck;
    }

    public Boolean getPersonalInfoExamPassed() {
        return personalInfoExamPassed;
    }

    public void setPersonalInfoExamPassed(Boolean personalInfoExamPassed) {
        this.personalInfoExamPassed = personalInfoExamPassed;
    }

    @Override
    public String toString() {
        return "PersonalInfo{" +
                "personalInfoCodeName='" + personalInfoCodeName + '\'' +
                ", personalInfoFirstName='" + personalInfoFirstName + '\'' +
                ", personalInfoLastName='" + personalInfoLastName + '\'' +
                ", personalInfoAccountBalance=" + personalInfoAccountBalance +
                ", personalInfoGearCheck=" + personalInfoGearCheck +
                ", personalInfoExamPassed=" + personalInfoExamPassed +
                '}';
    }
}
