package ee.bcskoolitus.fly.controller;

import ee.bcskoolitus.fly.dao.PersonalInfo;
import ee.bcskoolitus.fly.resources.resources.PersonalInfoResources;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/personalInfo")
public class PersonalInfoController {


    @GET
    @Path("/{name}")
    public PersonalInfo selectOnePerson(@PathParam("name") String personalInfoFirstName) {
        return PersonalInfoResources.selectOnePerson(personalInfoFirstName);
    }

    @GET
    @Path("/filter/{autoname}")
    public List<PersonalInfo> selectAutocompleteName(@PathParam("autoname") String autoName) {
        return PersonalInfoResources.autocompleteName(autoName);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public PersonalInfo addPersonalInfo(PersonalInfo newPersonalInfo) {
        return PersonalInfoResources.addPersonalInfo(newPersonalInfo);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public boolean updatePersonalInfo(PersonalInfo personalInfoUpdate) {
        return PersonalInfoResources.modifyPersonalInfo(personalInfoUpdate);
    }
}



