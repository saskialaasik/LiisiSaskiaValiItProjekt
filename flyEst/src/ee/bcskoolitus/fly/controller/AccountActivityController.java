package ee.bcskoolitus.fly.controller;

import ee.bcskoolitus.fly.dao.AccountActivity;

import ee.bcskoolitus.fly.resources.resources.AccountActivityResources;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/accountActivity")
public class AccountActivityController {


    @GET
    @Path("/{client}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<AccountActivity> selectOnePersonAccountHistory(@PathParam("client") String client) {
        return AccountActivityResources.displayPersonalAccountHistory(client);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public AccountActivity addNewTransaction(AccountActivity newTransaction) {
        return AccountActivityResources.addAccountActivity(newTransaction);
    }
}
