var onePersonInfo;

function printNewMemberOut(name) {
    $.getJSON("http://localhost:8080/rest/personalInfo/" + name,
        function (personalInfo) {
            onePersonInfo = personalInfo;
            var tableBodyContent = "";
            tableBodyContent = tableBodyContent
                + "<tr><td>Code Name</td></td><td>" + personalInfo.personalInfoCodeName + "</td></tr>"
                + "<tr><td>First Name</td><td>" + personalInfo.personalInfoFirstName + "</td></tr>"
                + "<tr><td>Last Name</td><td>" + personalInfo.personalInfoLastName + "</td></tr>"
                + "<tr><td>Account Balance</td><td>" + personalInfo.personalInfoAccountBalance + " €" + "</td></tr>"
                + "<tr><td>Gear Check</td><td>" + personalInfo.personalInfoGearCheck + "</td></tr>"
                + "<tr><td>Exam Passed</td><td>" + personalInfo.personalInfoExamPassed + "</td></tr>";

            document.getElementById("personalInfoTableBody").innerHTML = tableBodyContent;

        });
}

function printOutOnePersonInfo() {
    printNewMemberOut(document.getElementById("searchByName").value)
}

/*
(PersonalInfo.personalInfoCode.equals(newPersonalInfoCodeName)){//lisa controlleris tehtav funktsioon selle asemel koos ajaxiga
    alert("Code name already exists, please choose a new one.");codename
    */

/*
function saveNewPersonalInfo() {
    var codename = document.getElementById("newPersonalInfoCodeName").value;
    $.get("http://localhost:8080/rest/personalInfo/test/" + codename,
        function (isCodeNameUnique) {
            if (isCodeNameUnique == "false") {
                alert("Code name already exists, please choose a new one.");
            } else {
                withNewUniqueCodeNameSaveNewPersonalInfo();
            }
        });
}
*/


function withNewUniqueCodeNameSaveNewPersonalInfo() {
    var jsonData = {

        "personalInfoCodeName": document.getElementById("newPersonalInfoCodeName").value,
        "personalInfoFirstName": document.getElementById("newPersonalInfoFirstName").value,
        "personalInfoLastName": document.getElementById("newPersonalInfoLastName").value,
        "personalInfoAccountBalance": document.getElementById("newPersonalInfoAccountBalance").value, //number
        "personalInfoGearCheck": document.getElementById("newPersonalInfoGearCheck").checked ? 1 : 0, //boolean
        "personalInfoExamPassed": document.getElementById("newPersonalInfoExamPassed").checked ? 1 : 0 //boolean
    };

    $.ajax({
        url: "http://localhost:8080/rest/personalInfo",
        type: "POST",
        data: JSON.stringify(jsonData),
        contentType: "application/json; charset=utf-8",
        success: function () {
            clearPersonalData();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on adding new personal info.");
        }
    });

    function clearPersonalData() {
        document.getElementById("newPersonalInfoCodeName").value = "";
        document.getElementById("newPersonalInfoFirstName").value = "";
        document.getElementById("newPersonalInfoLastName").value = "";
        document.getElementById("newPersonalInfoAccountBalance").value = "";
        document.getElementById("newPersonalInfoGearCheck").value = "";
        document.getElementById("newPersonalInfoExamPassed").value = "";
    }
}

//Account Activity
var onePersonAccountHistory;

function selectOnePersonAccountHistory(client) {
    $.getJSON("http://localhost:8080/rest/accountActivity/" + client,
        function (personalAccountHistory) {
            onePersonAccountHistory = personalAccountHistory;
            var tableBodyContent = "";
            for (var i = 0; i < personalAccountHistory.length; i++) {
                tableBodyContent = tableBodyContent
                    + "<tr><td>" + personalAccountHistory[i].accountActivityDate.split("T")[0] + "</td>"
                    + "<td>" + personalAccountHistory[i].accountActivityClient + "</td>"
                    + "<td>" + personalAccountHistory[i].accountActivityBeneficiary + "</td>"
                    + "<td>" + personalAccountHistory[i].accountActivityAmount + " €" + "</td>"
                    + "<td>" + personalAccountHistory[i].accountActivityAccountBalanceAfter + " €" + "</td>"
                    + "<td>" + personalAccountHistory[i].accountActivityDetails + "</td></tr>";
            }
            document.getElementById("accountActivityTableBody").innerHTML = tableBodyContent;

        });
}

function printOutOnePersonAccountHistory() {
    selectOnePersonAccountHistory(document.getElementById("searchByClient").value)

}

//Transactions
function saveNewTransaction() {
    var jsonData = {
        "accountActivityClient": document.getElementById("newAccountActivityClient").value,
        "accountActivityBeneficiary": document.getElementById("newAccountActivityBeneficiary").value,
        "accountActivityAmount": document.getElementById("newAccountActivityAmount").value,
        "accountActivityDetails": document.getElementById("newAccountActivityDetails").value,
        "accountActivityDate": document.getElementById("newAccountActivityDate").value + "T00:00:00+02:00",
    };

    $.ajax({
        url: "http://localhost:8080/rest/accountActivity/",
        type: "POST",
        data: JSON.stringify(jsonData),
        contentType: "application/json; charset=utf-8",
        success: function () {
            clearAccountData();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on adding new transaction");
        }

    });

    function clearAccountData() {
        document.getElementById("newAccountActivityClient").value = "";
        document.getElementById("newAccountActivityBeneficiary").value = "";
        document.getElementById("newAccountActivityAmount").value = "";
        document.getElementById("newAccountActivityDetails").value = "";
        document.getElementById("newAccountActivityDate").value = "";
    }

}

/*
function validatePass() {
    if (document.getElementById('password').value == '123') {
        return true;
    } else {
        alert('wrong password!!');
        return false;
    }
}*/

$(document).ready(function () {
    $("#searchByName").on("keyup", function (e) {
        var key = e.keyCode;
        if (key === 13) {
            $("#nupp").click();
        }
        autocompleteName();
    });
});

function autocompleteName() {
    $(function () {
        $("#searchByName").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "http://localhost:8080/rest/personalInfo/filter/" + document.getElementById("searchByName").value,
                    type: "GET",
                    dataType: "json",

                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: ((item.personalInfoCodeName).concat(" ").concat(item.personalInfoFirstName)),
                                value: item.personalInfoCodeName
                            };
                        }));
                    }
                });
            },
            minLength: 2
        });
    });
}

$(document).ready(function () {
    $("#searchByClient").on("keyup", function (e) {
        var key = e.keyCode;
        if (key === 13) {
            $("#nupp").click();
        }
    });
});


function getPersonalInfoToModify(name) {
    $.getJSON("http://localhost:8080/rest/personalInfo/" + name,
        function (personalInfo) {
            onePersonInfo = personalInfo;
            document.getElementById("modifyPersonalInfoCodeName").value = onePersonInfo.personalInfoCodeName;
            document.getElementById("modifyPersonalInfoFirstName").value = onePersonInfo.personalInfoFirstName;
            document.getElementById("modifyPersonalInfoLastName").value = onePersonInfo.personalInfoLastName;
            document.getElementById("modifyPersonalInfoAccountBalance").value = onePersonInfo.personalInfoAccountBalance;
            document.getElementById("modifyPersonalInfoGearCheck").checked = onePersonInfo.personalInfoGearCheck;
            document.getElementById("modifyPersonalInfoExamPassed").checked = onePersonInfo.personalInfoExamPassed;
        });
}

function modifyOnePersonInfo() {
    getPersonalInfoToModify(document.getElementById("modifyByName").value)
}


function savePersonalInfoModifications() {
    var jsonData = {
        "personalInfoCodeName": document.getElementById("modifyPersonalInfoCodeName").value,
        "personalInfoFirstName": document.getElementById("modifyPersonalInfoFirstName").value,
        "personalInfoLastName": document.getElementById("modifyPersonalInfoLastName").value,
        "personalInfoAccountBalance": document.getElementById("modifyPersonalInfoAccountBalance").value, //number
        "personalInfoGearCheck": document.getElementById("modifyPersonalInfoGearCheck").checked ? 1 : 0, //boolean
        "personalInfoExamPassed": document.getElementById("modifyPersonalInfoExamPassed").checked ? 1 : 0 //boolean
    };

    var jsonDataString = JSON.stringify(jsonData);

    $.ajax({
        url: "http://localhost:8080/rest/personalInfo",
        type: "PUT",
        data: jsonDataString,
        contentType: "application/json; charset=utf-8"

    });
}

